#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <list>
#include <string>
#include <cstddef>

#include "node.h"
#include "in_out.h"

inline void die(const std::string &msg) {
    std::cerr << msg << std::endl;
    exit(1);
}

void print_v(std::vector<bool> vec, std::ofstream &output){
    for (auto elem : vec){
        output << elem;
    }
}

void print_func(std::vector<bool> vec, OutputBitStream &output, size_t& size){
    size +=vec.size();
    for (int i = 0; i < vec.size(); i++)
        output.write_bit(vec[i] ? 1 : 0);
}

bool sort_node(const Node *a, const Node *b) {
	return a->get_freq() < b->get_freq();
}


int main(int argc, char *argv[]) {
    if (argc != 4) die("Usage: ./cppfile InputFile OutputFile.");

    std::ifstream input_file(argv[1]);
    if (!input_file.good()) die("Failed to open input file.");

    std::ofstream output_file_code(argv[2], std::ifstream::binary);
    if (!output_file_code.good()) die("Failed to open output file.");

    std::ofstream output_file_dct(argv[3]);
    if (!output_file_dct.good()) die("Failed to open output file.");

    std::map<char, int> map_dictionary;
    char letter;
    while (!input_file.eof()){
        input_file.get(letter);
        if (input_file.eof()) break;
        auto it = map_dictionary.find(letter);
        if (it != map_dictionary.end()) 
            it->second += 1;
        else
            map_dictionary.insert(std::pair<char, int>(letter, 1));
    }

    std::list<Node*> tree_node;
    for (auto elem : map_dictionary){
        auto n = new Node(elem.first, elem.second);
        tree_node.push_back(n);
    }

    while (tree_node.size() != 1){
        tree_node.sort(sort_node);

        Node* l = tree_node.front();
        tree_node.pop_front();
        Node* r = tree_node.front();
        tree_node.pop_front();
        
        Node* p = new Node (l, r);
        tree_node.push_back(p);
    }

    Node *root = tree_node.front();
	std::vector<bool> code;
	std::map<char, std::vector<bool> > table;
	root->build_table(code, table); 

    for (auto elem : table){
        output_file_dct << (int)elem.first << " ";
        print_v(elem.second, output_file_dct);
        output_file_dct << "\n";
    }

    input_file.clear();
    input_file.seekg (0, input_file.beg);

    size_t size = 0;
    OutputBitStream bits(output_file_code);
    while (!input_file.eof()){
        input_file.get(letter);
        if (input_file.eof()) break;
        print_func(table[letter], bits, size);
    }
    bits.flush();
    output_file_dct << size;
    input_file.close();
    output_file_dct.close();
    output_file_code.close();

    return 0;
}
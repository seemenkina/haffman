#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>

#include "node.h"
#include "in_out.h"

inline void die(const std::string &msg) {
    std::cerr << msg << std::endl;
    exit(1);
}

void print_v(std::vector<bool> vec, std::ostream &output){
    for (auto elem : vec){
        output << elem;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 4) die("Usage: ./cppfile InputFile OutputFile.");

    std::ifstream input_file_code(argv[1], std::ifstream::binary);
    if (!input_file_code.good()) die("Failed to open input file.");

    std::ifstream input_file_dct(argv[2]);
    if (!input_file_dct.good()) die("Failed to open output file.");
    
    std::ofstream output_file_decode(argv[3]);
    if (!output_file_decode.good()) die("Failed to open output file.");

	std::map<std::vector<bool>, char> out_table;
    int l;
    std::string s;
    while (!input_file_dct.eof()){
        input_file_dct >> l >> s;
        if (input_file_dct.eof()) break;
        std::vector<bool> code;
        for (int i = 0; i < s.length(); i++)
            code.push_back(s[i] == '0' ? false : true);
        out_table.insert(std::pair<std::vector <bool>, char>(code, l));
        code.clear();
    }
    auto size_in_bit = l;
    std::vector<bool> buf;
    char c;
    auto in = InputBitStream(input_file_code);
    while (!input_file_code.eof() && size_in_bit > 0){
        auto bit = in.read_bit();
        // printf("%d ", bit)
        // std::cout << bit << "bit\n";
        //input_file_code.get(c);
        if (input_file_code.eof()) break;
        buf.push_back((bit & 1) ? true : false);
        // std::cout << "\n";
        if (out_table[buf]){
            output_file_decode << out_table[buf];
            buf.clear();
        }
        size_in_bit--;
    }

    input_file_code.close();
    input_file_dct.close();
    output_file_decode.close();

    return 0;
}
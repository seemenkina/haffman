#ifndef IN_OUT_H
#define IN_OUT_H

#include <iostream>
#include <cstdio>

class InputBitStream {
        std::istream& stream;
        uint8_t buf;
        size_t len;
public:
        InputBitStream(std::istream& s): stream(s), buf(0), len(0) {};

        uint8_t read_bit(){
            if (len == 0){
                buf = stream.get();
                buf = reverse(buf);
                len = 8;
            }
            uint8_t bit = buf & 1;
            buf >>= 1;
            len --;
            return bit;
        }

        uint8_t reverse(uint8_t v) {
            v = (v & 0xF0) >> 4 | (v & 0x0F) << 4;
            v = (v & 0xCC) >> 2 | (v & 0x33) << 2;
            v = (v & 0xAA) >> 1 | (v & 0x55) << 1;
            return v;
        }


};


class OutputBitStream {
    std::ostream& stream;
    uint8_t buf;
    size_t len;

public:
    OutputBitStream(std::ostream& stream): stream(stream), buf(0), len(0) {};
   
    void write_bit(uint8_t v) {
        if (len == 8) {
            flush();
        }
        uint8_t bit = v & 1;
        buf = ((uint8_t)(buf << 1)) | bit;
        len += 1;
    }
   
    void flush() {
        if (len != 8) {
            buf <<= (8 - len);
        }
        stream.put(buf);
        len = 0;
    }
};


#endif

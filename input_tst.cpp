//
// Created by Oleg Broslavsky on 21.06.2018.
//

#include <iostream>
#include "in_out.h"

int main(int argc, char *argv[]) {
    auto in =   InputBitStream(std::cin);
    auto out =  OutputBitStream(std::cout);

    for(int i = 0; i < 22; i++) {
        auto bit = in.read_bit();
        out.write_bit(bit);
    }
    out.flush();

    return 0;
}
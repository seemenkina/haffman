all_coder: haffman_coder.cpp
	g++ haffman_coder.cpp -o hc
all_decoder: haffman_decoder.cpp
	g++ haffman_decoder.cpp -o hd
run_decoder: all_decoder
	./hd output_code.txt output_dict.txt output_decode.txt
run_coder: all_coder
	./hc input.txt output_code.txt output_dict.txt

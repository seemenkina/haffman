#ifndef NODE_H
#define NODE_H

#include <fstream>

class Node {
private:
    char c;
    Node* left = nullptr;
    Node* right = nullptr;
    int freq = 0;

public:

    Node() {};

    Node(char ch, int f) : c(ch), freq(f) {};

    Node(Node* l, Node* r){
        c = 0;
        left = l;
        right = r;
        freq = left->freq + right->freq;
    }

    int get_freq() const{
        return freq;
    }

    void build_table(std::vector<bool> &code, std::map<char, std::vector<bool>> &table) {
	    if (left){
		    code.push_back(0); // left
		    left->build_table(code, table);
	    }
	    if (right){
		    code.push_back(1); // right
		    right->build_table(code, table);
	    }
	    if (c)
		    table[c] = code;
	    if (code.size())
            code.pop_back();
    }
};

#endif